DELETE FROM `tblconcepto` WHERE (`codConcepto`='1001');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1002');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1003');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1004');
DELETE FROM `parametro` WHERE (`concepto_id`='1001');
DELETE FROM `parametro` WHERE (`concepto_id`='1002');
DELETE FROM `parametro` WHERE (`concepto_id`='1003')
DELETE FROM `parametro` WHERE (`concepto_id`='1004')


DROP TABLE IF EXISTS cliente;

DROP PROCEDURE IF EXISTS pa_insert_cliente;
DELIMITER ;;

DROP PROCEDURE IF EXISTS pa_update_cliente;
DELIMITER ;;

DROP PROCEDURE IF EXISTS `pa_listar_conceptos`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_conceptos`(IN `flag` INT)
begin
	
	IF flag = 1 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0 and correlativo<07 order by correlativo ASC;

      END IF;
	

	  IF flag = 2 then

		    SELECT concat(ruc,"|",ruta_dowland,"|",razon_social,"|",telefono,"|",celular,"|",direccion) as descripcion FROM empresa limit 1;
   
	   END IF;

	    IF flag = 3 then

		    SELECT parametro as descripcion FROM parametro where concepto_id = 1 and marca_baja = 0;
  
	    END IF;
  
	    IF flag = 4 then

		    SELECT parametro as descripcion FROM parametro where concepto_id = 2 and marca_baja = 0;
  
	    END IF;


	IF flag = 5 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0 and correlativo >06  and correlativo<09 order by correlativo ASC;

      END IF;

	IF flag = 6 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =20  and parametro2 ="01"and marca_baja = 0;

      END IF;

	IF flag = 7 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =20  and parametro2 ="03"and marca_baja = 0;

      END IF;

	IF flag = 8 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =21  and parametro2 ="01"and marca_baja = 0;

      END IF;

	IF flag = 9 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =21  and parametro2 ="03"and marca_baja = 0;

      END IF;
	IF flag = 10 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 2   AND marcaBaja = 0;

      END IF;
	IF flag = 11 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 3   AND marcaBaja = 0 ;

      END IF;

	IF flag = 12 then

		    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0  order by correlativo ASC;

      END IF;

	IF flag = 13 then
		   SELECT  parametro2 as codigo, parametro as descripcion  FROM `parametro`where concepto_id=28 and marca_baja=0;
      END IF;

	IF flag = 14 then
		   SELECT parametro2 as codigo, parametro as descripcion  FROM `parametro`where concepto_id=29 and marca_baja=0;
      END IF;
	IF flag = 15 then
		   SELECT parametro2 as codigo, parametro as descripcion, par_tipo as free  FROM `parametro`where concepto_id=30 and marca_baja=0;
      END IF;
	IF flag = 16 then
		   SELECT   par_tipo as free  FROM `parametro`where concepto_id=32 and marca_baja=0;
      END IF;
    IF flag = 17 then
		SELECT GROUP_CONCAT(par_int1) FROM `parametro` where concepto_id=33 and marca_baja=0;
	END IF;
	IF flag = 18 then
		    SELECT  par_tipo as activoCodigoSunat  FROM `parametro`where concepto_id=34 and marca_baja=0;
    END IF;
	IF flag = 19 then
	    SELECT  par_tipo as activoPrdCatVenta  FROM `parametro`where concepto_id=35 and marca_baja=0;
	 END IF;
end
;;
DELIMITER ;


DROP PROCEDURE IF EXISTS `pa_listar_data_comprobante`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_data_comprobante`(`_id` INT)
BEGIN
SELECT
 '0101' as TipoOperacion,									-- (01 : Tipo de operación) Catálogo N° 51 , 0101 - venta interna
       cast( cab.fecha_emision as char(10)) as FechaEmision,	-- (02 : Fecha de emisión YYYY-MM-DD
	   '05:02:12' AS HoraEmision,								-- (03 : Hora de Emisión ) HH:MM:SS
	   case when cab.fecha_vencimiento is null then '-' 
	   else cast( cab.fecha_vencimiento as char(10)) End
	   as FechaVencimiento,										-- (04 :Fecha de vencimiento ) YYYY-MM-DD
	   '0000' CodDomicilioFiscal,								-- (05 : Código del domicilio fiscal o de local anexo del emisor )
	   cab.tipo_documento as TipoDocCliente,				    -- (06 : Tipo de documento de identidad del adquirente o usuario) Catálogo N° 6
	    cab.nom_cliente as nomCustomer,	
		cab.num_documento as docCustomer,						-- (08 : Apellidos y nombres, denominación o razón social del adquirente o usuario   )
	   p.parametro2 as tipo_moneda,									-- (09 : Tipo de moneda en la cual se emite la factura electrónica) Catálogo N° 2
	   cab.igv + cab.isc as SumTotalTributos,			        -- (10 : Sumatoria Tributos)
	   cab.subtotal as TotalValorVenta,						    -- (11 : Total valor de venta )
	   cab.importe as TotalPrecioVenta,					        -- (12 : Total Precio de Venta)
	   (cab.otros_descuentos + cab.descuento_global) as TotalDescuentos,	-- (13 : Total descuentos (no afectan la base imponible del IGV/IVAP))
	  '0.00' as otrosCargos,
      '0.00' as TotalAnticipos,				                        -- (15 : Total Anticipos)
	   cab.importe as ImporteTotalVenta,				        -- (16 : Importe total de la venta, cesión en uso o del servicio prestado, suma de 12-13+14-15)
	   '2.1' as VersionUBL,										-- (17 : Versión UBL)
	   '2.0' as Customizacion,	                                -- (18 : Customization Documento) 

		cab.codigo as seriesAfectado,
		cab.dir_cliente as direccionCliente,
		cab.tipo_comprobante as tipoComprobante

       FROM comprobante as cab, empresa as emp, parametro as p
where emp.id = cab.empresa_id and cab.id = _id  and p.concepto_id = 28
and p.parametro2 = cab.tipo_moneda;


Select 
	'NIU' as UnidadMedida,	               
       	dt.cantidad as cantidad,						     
	IF(dt.codigo_producto="","-",dt.codigo_producto) as codigoProducto,					
	 IF(dt.codigo_sunat IS NULL,"-",dt.codigo_sunat) as CodigoProdSunat,					
	 dt.producto as Producto,		            
	 dt.valor_item as ValorUnitario,	
			      
	ROUND( (dt.precio_unitario * dt.cantidad * 0.18/1.18),2) as SumTribxItem,	/*cambios mal calculado*/
	  
    
	   '1000' as CodTribIGV,		            
	   ROUND(dt.valor_item * 0.18,2) as MontoIGVItem,	
	   dt.valor_item as baseImponible,						
	   'IGV' as nombreTributo,	

		'VAT' as codAfecIGV,			      

	   '10' as tributoAfectacion,				                
	   '18.00' as tributoPorcentaje ,								

	  
	    '-' as codigTributoISC,					
	    0 as mtoISCxitem,		-- (16 :
		0 as BaseImpISC,									
		'' as NomTribxItemISC,					
		'' as CodTiposTributoISC,					
		-- CodTipoTributoISC,Tributo ISC: Porcentaje de ISC

		'' as TipoSistemaISC,								
		'' as PorcImoISC,									
		-- Tributo Otro 9999
		'-' as CodTipoTribOtros,							
		0 as MtoTribOTrosxItem,							
		0 as BaseImpOtroxItem,							
		'' as NomTribOtroxItem,							
		'' as CodTipTribOtroxItem,						
		0 as PorTribOtroXItem,							
		-- Tributo ICBPER 7152
		'-' as codTriIcbper,							
		'' as mtoTriIcbperItem,						
		'' as ctdBolsasTriIcbperItem,				
		'' as nomTributoIcbperItem,					
		'' as codTipTributoIcbperItem,				
		'' as mtoTriIcbperUnidad ,					
		--

		(dt.precio_unitario ) as PrecioVtaUnitario,		
		
		dt.valor_item as valor_item	,						
		0 as ValorRefUnitario_Gratuito,

		 ROUND(dt.precio_unitario/1.18,2 ) as valorBaseItem	,
		dt.unidad_medida as unidadMedidaNota,
		dt.codigo_producto as codigoProductoNota				
FROM detalle_comprobante as dt,comprobante as cmp
where dt.comprobante_id = cmp.id and dt.comprobante_id = _id;
end
;;
DELIMITER ;



DROP PROCEDURE IF EXISTS `pa_search_comprobantes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_comprobantes`(
 IN  codigo_venta  int,
 IN series_number varchar(50),

 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 )
BEGIN
		if series_number is NULL OR series_number='' then
			SELECT 
			c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
			 FROM `comprobante` as c
			where c.tipo_comprobante in ('01','03') and 

			 c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )
			and
			( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			order by c.id desc;
 		else 
			SELECT 
			c.id as VENTA,  c.codigo as DOCUMENTO ,c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
			 FROM `comprobante`c
			where 
			c.tipo_comprobante in ('01','03') and		
			 c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )		

			and	c.codigo  = series_number 	
			and   if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   ) order by c.id desc;

		end if ;
						   			

 END

;;
DELIMITER ;



DROP PROCEDURE IF EXISTS `pa_search_comprobantes_fechas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_comprobantes_fechas`(
 IN fecha_inicio   varchar(50),   
IN fecha_fin  varchar(50),
 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 , IN codigo_venta int,  IN series_number varchar(50))
BEGIN
		SELECT 
			c.id as VENTA, c.codigo as DOCUMENTO,c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
		 FROM `comprobante` AS c
		where 
		c.tipo_comprobante in ('01','03') 
		and   c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )
		and	( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
		and  c.fecha_emision  between fecha_inicio and fecha_fin 
		and	if( (series_number is null || series_number="") , c.codigo like"%%" ,c.codigo  = series_number ) 
		and   if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   ) 
		order by c.id desc;
	
END
;;
DELIMITER ;






