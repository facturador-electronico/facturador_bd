
DROP TABLE IF EXISTS `comprobante`;
CREATE TABLE `comprobante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_emision` date NOT NULL,
  `fecha_vencimiento` date DEFAULT NULL,
  `codigo` varchar(20) DEFAULT NULL,
  `num_documento` varchar(11) DEFAULT NULL,
  `tipo_documento` char(1) NOT NULL,
  `tipo_moneda` varchar(20) DEFAULT NULL,
  `dir_cliente` text DEFAULT NULL,
  `nom_cliente` varchar(120) DEFAULT NULL,
  `igv` decimal(11,2) NOT NULL,
  `isc` decimal(11,2) NOT NULL,
  `otros_descuentos` decimal(11,2) NOT NULL,
  `descuento_global` decimal(11,2) NOT NULL,
  `subtotal` decimal(11,2) NOT NULL,
  `descuento_total` decimal(11,2) NOT NULL,
  `anticipo` decimal(11,2) NOT NULL,
  `importe` decimal(11,2) NOT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `tipo_comprobante` varchar(5) NOT NULL,
  `tipo_operacion` char(3) NOT NULL,
  `id_estado` char(3) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
   -- campos de auditoria
  `marca_baja` smallint NOT NULL DEFAULT 0,
  `fecha_transaccion` date DEFAULT NULL ,
  `fecha_proceso` date DEFAULT NOW(),
  `hora_proceso` time DEFAULT DATE_FORMAT(NOW(), "%H:%i:%S"),
  `usuario` varchar(255) DEFAULT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `detalle_comprobante`
-- ----------------------------
DROP TABLE IF EXISTS `detalle_comprobante`;
CREATE TABLE `detalle_comprobante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` decimal(11,2) NOT NULL,
  `unidad_medida` varchar(20) NOT NULL,
  `codigo_producto` varchar(10) NOT NULL,
  `codigo_sunat` varchar(10) null,
  `producto` varchar(255) NOT NULL,
  `precio_unitario` decimal(11,2) NOT NULL,
  `descuento_unitario` decimal(11,2) NOT NULL,
  `valor_item` decimal(11,2) NOT NULL,
  `icbper` decimal(11,2) NOT NULL,
  `comprobante_id` int(11) NOT NULL,
     -- campos de auditoria
  `marca_baja` smallint NOT NULL DEFAULT 0,
  `fecha_transaccion` date DEFAULT NULL ,
  `fecha_proceso` date DEFAULT NOW(),
  `hora_proceso` time DEFAULT DATE_FORMAT(NOW(), "%H:%i:%S"),
  `usuario` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `empresa`
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruc` varchar(12) NOT NULL,
  `razon_social` varchar(255) NOT NULL,
  `ruta_dowland` text NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `celular` varchar(12) NOT NULL,
  `usuario_soap` varchar(25) NOT NULL,
   `clave_soap` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `notes`
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `document_id` int(10) unsigned NOT NULL,
  `importe` decimal(12,2) NOT NULL,
  `note_type` enum('credit','debit') NOT NULL,
  `note_credit_type_id` varchar(255) DEFAULT NULL,
  `note_debit_type_id` varchar(255) DEFAULT NULL,
  `note_description` varchar(255) NOT NULL,
  `affected_document_id` int(10) unsigned DEFAULT NULL,
  `marca_baja` smallint(1) NOT NULL,
  `fecha_transaccion` date DEFAULT NULL,
  `fecha_proceso` date DEFAULT NULL,
  `hora_proceso` time DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


-- ----------------------------
--  Table structure for `parametro`
-- ----------------------------
DROP TABLE IF EXISTS `parametro`;
CREATE TABLE `parametro` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `concepto_id` int(11) NOT NULL,
  `parametro` varchar(250) DEFAULT NULL,
  `parametro2` varchar(250) DEFAULT NULL,
  `par_int1` int(11) DEFAULT NULL,
  `par_int2` int(11) DEFAULT NULL,
  `par_float1` float DEFAULT NULL,
  `par_float2` float DEFAULT NULL,
  `par_date1` datetime DEFAULT NULL,
  `par_date2` datetime DEFAULT NULL,
  `par_tipo` int(5) DEFAULT NULL,
  `marca_baja` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `producto`
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `unidad_medida_id` varchar(20) NOT NULL,
  `moneda_id` varchar(20) NOT NULL,
  `afectacion_venta_id` int(11) NOT NULL,
  `afectacion_compra_id` int(11) DEFAULT NULL,
  `precio_venta` float(11,2) NOT NULL,
  `precio_compra` decimal(11,2) DEFAULT NULL,
  `codigo_interno` varchar(255) DEFAULT NULL,
  `codigo_sunat` varchar(8) DEFAULT '',
  `has_igv` char(1) DEFAULT '',
  `marca_baja` tinyint(4) NOT NULL DEFAULT 0,
  `fecha_proceso` datetime DEFAULT NULL,
  `fecha_transaccion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `tblconcepto`
-- ----------------------------
DROP TABLE IF EXISTS `tblconcepto`;
CREATE TABLE `tblconcepto` (
  `ntraConcepto` int(11) NOT NULL AUTO_INCREMENT,
  `codConcepto` int(11) NOT NULL,
  `correlativo` varchar(20) NOT NULL,
  `decripcion2` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `marcaBaja` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ntraConcepto`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;


-- ----------------------------
--  Table structure for `codsntclase`
-- ----------------------------
DROP TABLE IF EXISTS `codsntclase`;
CREATE TABLE `codsntclase` (
  `id` int(11) NOT NULL,
  `id_familia` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `marca_baja` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `codsntfamilia`
-- ----------------------------
DROP TABLE IF EXISTS `codsntfamilia`;
CREATE TABLE `codsntfamilia` (
  `id` int(11) NOT NULL,
  `id_segmento` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `marca_baja` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `codsntsegmento`
-- ----------------------------
DROP TABLE IF EXISTS `codsntsegmento`;
CREATE TABLE `codsntsegmento` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `marca_baja` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `codsntproducto`
-- ----------------------------
DROP TABLE IF EXISTS `codsntproducto`;
CREATE TABLE `codsntproducto` (
  `id` int(11) NOT NULL,
  `id_clase` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `marca_baja` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


