-- ----------------------------
--  Procedure definition for `generar_codigo_facturas`
-- ----------------------------
DROP PROCEDURE IF EXISTS `generar_codigo_facturas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generar_codigo_facturas`(`_tipo_comprobante` VARCHAR(5))
BEGIN
	
    DECLARE cant int; 
    
    select count(tipo_comprobante) + 1  INTO cant from comprobante 
    where tipo_comprobante = _tipo_comprobante;
    
    
	SELECT lpad(cant,8,0);

END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_buscar_documento_afectado_nota`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_buscar_documento_afectado_nota`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_documento_afectado_nota`(IN `_id` int)
BEGIN
	#Routine body goes here...

	

select 
c.codigo as codigo_afectado, 
if(cnc.description is null, "",cnc.description) as reason_debit,
if(cdc.description is null, "",cdc.description) as reason_debito
from 
notes n 
left join comprobante c on  n.affected_document_id=c.id
left join cat_note_credit_types cnc on n.note_credit_type_id  =cnc.id
left join cat_note_debit_types cdc on n.note_debit_type_id =cdc.id
where n.document_id=_id;

END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_generar_codigo_notas`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_generar_codigo_notas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_generar_codigo_notas`(IN `_tipo_comprobante` varchar(5),IN `_inicial` char(1))
BEGIN
	
    DECLARE cant int; 
    
    select count(tipo_comprobante) + 1  INTO cant from comprobante 
    where tipo_comprobante = _tipo_comprobante and codigo  like CONCAT('%', _inicial , '%');
    
    
	SELECT lpad(cant,8,0);

END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_get_hash`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_get_hash`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_get_hash`(IN `_id` int)
BEGIN
	#Routine body goes here...
select hash from comprobante where id =_id;
END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_insert_notes`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_insert_notes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insert_notes`(
IN document_id INT,
IN note_type varchar(50),
IN note_credit_type_id varchar(50),
IN note_debit_type_id varchar(50),
IN note_description varchar(255),
IN affected_document_id INT
 , IN marca_baja char(1), IN fecha_transaccion date, IN fecha_proceso date, IN hora_proceso time, IN usuario varchar(255), IN importe decimal(12,2))
BEGIN

INSERT INTO `notes` (
`document_id`, 
`importe`,
`note_type`,
`note_credit_type_id`,
`note_debit_type_id`, 
`note_description`, 
`affected_document_id`, 
`marca_baja`,
`fecha_transaccion`,
`fecha_proceso`,
`hora_proceso`,
`usuario`

)
 VALUES (

document_id,
importe,
note_type,
note_credit_type_id,
note_debit_type_id,
note_description,
affected_document_id,
marca_baja,
fecha_transaccion,
fecha_proceso,
hora_proceso,
usuario

);

END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_listar_conceptos`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_listar_conceptos`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_conceptos`(IN `flag` INT)
begin
	
	IF flag = 1 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0 and correlativo<07 order by correlativo ASC;

      END IF;
	

	  IF flag = 2 then

		    SELECT concat(ruc,"|",ruta_dowland,"|",razon_social,"|",telefono,"|",celular,"|",direccion) as descripcion FROM empresa limit 1;
   
	   END IF;

	    IF flag = 3 then

		    SELECT parametro as descripcion FROM parametro where concepto_id = 1 and marca_baja = 0;
  
	    END IF;
  
	    IF flag = 4 then

		    SELECT parametro as descripcion FROM parametro where concepto_id = 2 and marca_baja = 0;
  
	    END IF;


	IF flag = 5 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0 and correlativo >06  and correlativo<09 order by correlativo ASC;

      END IF;

	IF flag = 6 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =20  and parametro2 ="01"and marca_baja = 0;

      END IF;

	IF flag = 7 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =20  and parametro2 ="03"and marca_baja = 0;

      END IF;

	IF flag = 8 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =21  and parametro2 ="01"and marca_baja = 0;

      END IF;

	IF flag = 9 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =21  and parametro2 ="03"and marca_baja = 0;

      END IF;
	IF flag = 10 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 2   AND marcaBaja = 0;

      END IF;
	IF flag = 11 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 3   AND marcaBaja = 0 ;

      END IF;

	IF flag = 12 then

		    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0  order by correlativo ASC;

      END IF;

	IF flag = 13 then
		   SELECT  parametro2 as codigo, parametro as descripcion  FROM `parametro`where concepto_id=28 and marca_baja=0;
      END IF;

	IF flag = 14 then
		   SELECT parametro2 as codigo, parametro as descripcion  FROM `parametro`where concepto_id=29 and marca_baja=0;
      END IF;
	IF flag = 15 then
		   SELECT parametro2 as codigo, parametro as descripcion, par_tipo as free  FROM `parametro`where concepto_id=30 and marca_baja=0;
      END IF;
	IF flag = 16 then
		   SELECT   par_tipo as free  FROM `parametro`where concepto_id=32 and marca_baja=0;
      END IF;
    IF flag = 17 then
		SELECT GROUP_CONCAT(par_int1) FROM `parametro` where concepto_id=33 and marca_baja=0;
	END IF;
	IF flag = 18 then
		    SELECT  par_tipo as activoCodigoSunat  FROM `parametro`where concepto_id=34 and marca_baja=0;
    END IF;
	IF flag = 19 then
	    SELECT  par_tipo as activoPrdCatVenta  FROM `parametro`where concepto_id=35 and marca_baja=0;
	 END IF;
end
;;
DELIMITER ;


-- ----------------------------
--  Procedure definition for `pa_listar_data_comprobante`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_listar_data_comprobante`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_data_comprobante`(`_id` INT)
BEGIN
SELECT
 '0101' as TipoOperacion,									-- (01 : Tipo de operación) Catálogo N° 51 , 0101 - venta interna
       cast( cab.fecha_emision as char(10)) as FechaEmision,	-- (02 : Fecha de emisión YYYY-MM-DD
	   '05:02:12' AS HoraEmision,								-- (03 : Hora de Emisión ) HH:MM:SS
	   case when cab.fecha_vencimiento is null then '-' 
	   else cast( cab.fecha_vencimiento as char(10)) End
	   as FechaVencimiento,										-- (04 :Fecha de vencimiento ) YYYY-MM-DD
	   '0000' CodDomicilioFiscal,								-- (05 : Código del domicilio fiscal o de local anexo del emisor )
	   cab.tipo_documento as TipoDocCliente,				    -- (06 : Tipo de documento de identidad del adquirente o usuario) Catálogo N° 6
	    cab.nom_cliente as nomCustomer,	
		cab.num_documento as docCustomer,						-- (08 : Apellidos y nombres, denominación o razón social del adquirente o usuario   )
	   p.parametro2 as tipo_moneda,									-- (09 : Tipo de moneda en la cual se emite la factura electrónica) Catálogo N° 2
	   cab.igv + cab.isc as SumTotalTributos,			        -- (10 : Sumatoria Tributos)
	   cab.subtotal as TotalValorVenta,						    -- (11 : Total valor de venta )
	   cab.importe as TotalPrecioVenta,					        -- (12 : Total Precio de Venta)
	   (cab.otros_descuentos + cab.descuento_global) as TotalDescuentos,	-- (13 : Total descuentos (no afectan la base imponible del IGV/IVAP))
	  '0.00' as otrosCargos,
      '0.00' as TotalAnticipos,				                        -- (15 : Total Anticipos)
	   cab.importe as ImporteTotalVenta,				        -- (16 : Importe total de la venta, cesión en uso o del servicio prestado, suma de 12-13+14-15)
	   '2.1' as VersionUBL,										-- (17 : Versión UBL)
	   '2.0' as Customizacion,	                                -- (18 : Customization Documento) 

		cab.codigo as seriesAfectado,
		cab.dir_cliente as direccionCliente,
		cab.tipo_comprobante as tipoComprobante

       FROM comprobante as cab, empresa as emp, parametro as p
where emp.id = cab.empresa_id and cab.id = _id  and p.concepto_id = 28
and p.parametro2 = cab.tipo_moneda;


Select 
	'NIU' as UnidadMedida,	               
       	dt.cantidad as cantidad,						     
	IF(dt.codigo_producto="","-",dt.codigo_producto) as codigoProducto,					
	 IF(dt.codigo_sunat IS NULL,"-",dt.codigo_sunat) as CodigoProdSunat,					
	 dt.producto as Producto,		            
	 dt.valor_item as ValorUnitario,	
			      
	ROUND( (dt.precio_unitario * dt.cantidad * 0.18/1.18),2) as SumTribxItem,	/*cambios mal calculado*/
	  
    
	   '1000' as CodTribIGV,		            
	   ROUND(dt.valor_item * 0.18,2) as MontoIGVItem,	
	   dt.valor_item as baseImponible,						
	   'IGV' as nombreTributo,	

		'VAT' as codAfecIGV,			      

	   '10' as tributoAfectacion,				                
	   '18.00' as tributoPorcentaje ,								

	  
	    '-' as codigTributoISC,					
	    0 as mtoISCxitem,		-- (16 :
		0 as BaseImpISC,									
		'' as NomTribxItemISC,					
		'' as CodTiposTributoISC,					
		-- CodTipoTributoISC,Tributo ISC: Porcentaje de ISC

		'' as TipoSistemaISC,								
		'' as PorcImoISC,									
		-- Tributo Otro 9999
		'-' as CodTipoTribOtros,							
		0 as MtoTribOTrosxItem,							
		0 as BaseImpOtroxItem,							
		'' as NomTribOtroxItem,							
		'' as CodTipTribOtroxItem,						
		0 as PorTribOtroXItem,							
		-- Tributo ICBPER 7152
		'-' as codTriIcbper,							
		'' as mtoTriIcbperItem,						
		'' as ctdBolsasTriIcbperItem,				
		'' as nomTributoIcbperItem,					
		'' as codTipTributoIcbperItem,				
		'' as mtoTriIcbperUnidad ,					
		--

		(dt.precio_unitario ) as PrecioVtaUnitario,		
		
		dt.valor_item as valor_item	,						
		0 as ValorRefUnitario_Gratuito,

		 ROUND(dt.precio_unitario/1.18,2 ) as valorBaseItem	,
		dt.unidad_medida as unidadMedidaNota,
		dt.codigo_producto as codigoProductoNota				
FROM detalle_comprobante as dt,comprobante as cmp
where dt.comprobante_id = cmp.id and dt.comprobante_id = _id;
end
;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for `pa_search_comprobantes`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_search_comprobantes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_comprobantes`(
 IN  codigo_venta  int,
 IN series_number varchar(50),

 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 )
BEGIN
		if series_number is NULL OR series_number='' then
			SELECT 
			c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
			 FROM `comprobante` as c
			where c.tipo_comprobante in ('01','03') and 

			 c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )
			and
			( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			order by c.id desc;
 		else 
			SELECT 
			c.id as VENTA,  c.codigo as DOCUMENTO ,c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
			 FROM `comprobante`c
			where 
			c.tipo_comprobante in ('01','03') and		
			 c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )		

			and	c.codigo  = series_number 	
			and   if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   ) order by c.id desc;

		end if ;
						   			

 END

;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for `pa_search_comprobantes_fechas`
-- ----------------------------

DROP PROCEDURE IF EXISTS `pa_search_comprobantes_fechas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_comprobantes_fechas`(
 IN fecha_inicio   varchar(50),   
IN fecha_fin  varchar(50),
 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 , IN codigo_venta int,  IN series_number varchar(50))
BEGIN
		SELECT 
			c.id as VENTA, c.codigo as DOCUMENTO,c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
		 FROM `comprobante` AS c
		where 
		c.tipo_comprobante in ('01','03') 
		and   c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )
		and	( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
		and  c.fecha_emision  between fecha_inicio and fecha_fin 
		and	if( (series_number is null || series_number="") , c.codigo like"%%" ,c.codigo  = series_number ) 
		and   if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   ) 
		order by c.id desc;
	
END
;;
DELIMITER ;


-- ----------------------------
--  Procedure definition for `pa_search_listado_comprobantes`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_search_listado_comprobantes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_listado_comprobantes`(
 IN  codigo_venta  int,
 IN series_number varchar(50),

 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
, IN _tipo_comprobante char(2), IN _id_estado char(2))
BEGIN

			SELECT 
			c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE, 
			CASE c.id_estado
          				 WHEN '01'       THEN 'REGISTRADO'
          				 WHEN '02'       THEN 'XML GENERADO'
          				 WHEN '03'       THEN 'ENVIADO'
          				 WHEN '05'       THEN 'ACEPTADO'
					 WHEN '07'       THEN 'OBSERVADO'
					 WHEN '09'       THEN 'RECHAZADO'
		       END AS ESTADO,
			 c.tipo_moneda as MONEDA,
			if( c.subtotal <0  , (c.subtotal*-1)  , c.subtotal ) as GRAVADO, 
			if( c.igv <0  , (c.igv*-1)  , c.igv ) as IGV,
			if( c.importe <0  , (c.importe*-1)  , c.importe ) as TOTAL,
			(select descripcion from tblconcepto where codConcepto=1 and correlativo=c.tipo_comprobante ) as TIPO,
			c.tipo_comprobante as TIPO_COMPROBANTE

			 FROM `comprobante` as c
			where (c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and  if(series_number ="",c.codigo like "%%",c.codigo  = series_number )
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			and  if(_tipo_comprobante="0", c.tipo_comprobante  like"%%", c.tipo_comprobante =_tipo_comprobante)

			and  if(_id_estado ="",c.id_estado like "%%",c.id_estado  = _id_estado  )

			order by c.id desc;
 	

						   			

 END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_search_listado_comprobantes_fechas`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_search_listado_comprobantes_fechas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_listado_comprobantes_fechas`(
 IN fecha_inicio   varchar(50),   
IN fecha_fin  varchar(50),
 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 , IN _tipo_comprobante char(2),IN series_number varchar(50), IN codigo_venta int, IN _id_estado char(2))
BEGIN

		SELECT 
				c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE,
  				CASE c.id_estado
          			 	WHEN '01'       THEN 'REGISTRADO'
          				 WHEN '02'       THEN 'XML GENERADO'
          				 WHEN '03'       THEN 'ENVIADO'
          				 WHEN '05'       THEN 'ACEPTADO'
					 WHEN '07'       THEN 'OBSERVADO'
					 WHEN '09'       THEN 'RECHAZADO'
		       END AS ESTADO, c.tipo_moneda as MONEDA,
			if( c.subtotal <0  , (c.subtotal*-1)  , c.subtotal ) as GRAVADO, 
			if( c.igv <0  , (c.igv*-1)  , c.igv ) as IGV,
			if( c.importe <0  , (c.importe*-1)  , c.importe ) as TOTAL,
			(select descripcion from tblconcepto where codConcepto=1 and correlativo=c.tipo_comprobante ) as TIPO,
			c.tipo_comprobante as TIPO_COMPROBANTE
		 FROM `comprobante` AS c
		where 
		( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and  if(series_number ="",c.codigo like "%%",c.codigo  = series_number )
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			and  c.fecha_emision  between fecha_inicio and fecha_fin
			and  if(_tipo_comprobante="0", c.tipo_comprobante  like"%%", c.tipo_comprobante =_tipo_comprobante)

			and  if(_id_estado ="",c.id_estado like "%%",c.id_estado  = _id_estado  )
			  order by  c.id  desc;




		
	
END
;;
DELIMITER ;

-- ----------------------------
--  Procedure definition for `pa_update_hash`
-- ----------------------------
DROP PROCEDURE IF EXISTS `pa_update_hash`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_update_hash`(IN `_id` int,IN `_hash` varchar(255))
BEGIN
	#Routine body goes here...
update comprobante set hash=_hash where id=_id;
END
;;
DELIMITER ;







-- ----------------------------
--  Procedure definition for `pa_buscar_documento_afectado_nota`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  19-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: buscad el documento afectado por nota, y descripcion de anulacion
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_buscar_documento_afectado_nota`;
DELIMITER ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_documento_afectado_nota`(IN `_id` int)
BEGIN


select 
c.codigo as codigo_afectado, 
if(n.note_credit_type_id is null, "", 
(SELECT  descripcion   FROM `tblconcepto` where codConcepto=3  and correlativo=n.note_credit_type_id) ) as reason_credit,
if(n.note_debit_type_id is null, "", 
(SELECT  descripcion   FROM `tblconcepto` where codConcepto=2  and correlativo=n.note_debit_type_id)) as reason_debit
from 
notes n 
left join comprobante c on  n.affected_document_id=c.id
where n.document_id=_id;

END;;
DELIMITER ;


-- ----------------------------
--  Procedure definition for
-- `pa_buscar_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electr򭨣o
-- Modulo: General
-- Descripci򬸠busqueda de producto para listado y edicion
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_buscar_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_producto`(IN `_descripcion` varchar(255),IN `_codigo_interno` varchar(255), IN `_id` int)
BEGIN
	#Routine body goes here...
	SELECT 

p.codigo_interno,
p.descripcion,
(SELECT  parametro   FROM  parametro where concepto_id=29  and parametro2=p.unidad_medida_id)  as unidadMedida,
(SELECT  parametro   FROM  parametro where concepto_id=28  and parametro2=p.moneda_id)  as moneda,
(SELECT  parametro   FROM  parametro where concepto_id=30  and parametro2=p.afectacion_venta_id) as moneda,
p.precio_venta,
if(p.has_igv="1","SI","NO") as conIGV,
p.id, 
p.codigo_sunat, 
p.unidad_medida_id, 
p.moneda_id,
p.afectacion_venta_id
 FROM `producto` p
where 	
 p.descripcion REGEXP   _descripcion 
and p.codigo_interno like  CONCAT('%', _codigo_interno , '%')
and 
if(_id=0,p.id like "%%", p.id=_id)
and p.marca_baja=0
order by p.id desc;
 	
END;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for
-- `pa_eliminar_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: da de baja un producto
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_eliminar_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_eliminar_producto`( IN _id int, IN _fecha_transaccion datetime)
BEGIN
	update `producto` set marca_baja= 9, fecha_transaccion= _fecha_transaccion where id=_id;
END;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for
-- `pa_update_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: actualiza datos de tabla producto
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_update_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_update_producto`(IN `_descripcion` varchar(255),IN `_unidad_medida_id` varchar(20),IN `_moneda_id` varchar(20),IN `_afectacion_venta_id` int,IN `_afectacion_compra_id` int,IN `_precio_venta` decimal(11,2),IN `_precio_compra` decimal(11,2),IN `_codigo_interno` varchar(255),IN `_codigo_sunat` varchar(8),IN `_has_igv` char(1), IN _id int, IN _fecha_transaccion datetime)
BEGIN
	#Routine body goes here...


update `producto`
 set
descripcion = _descripcion,
unidad_medida_id = _unidad_medida_id,
moneda_id =_moneda_id,
afectacion_venta_id= _afectacion_venta_id ,
afectacion_compra_id =_afectacion_compra_id,
precio_venta = _precio_venta,
precio_compra=_precio_compra,
codigo_interno=_codigo_interno,
codigo_sunat=_codigo_sunat ,
has_igv= _has_igv ,
fecha_transaccion= _fecha_transaccion


where id=_id;

END;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for
-- `pa_insertar_productos`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: inserta datos en la tabla productos
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_insertar_productos`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insertar_productos`(IN `_descripcion` varchar(255),IN `_unidad_medida_id` varchar(20),IN `_moneda_id` varchar(20),IN `_afectacion_venta_id` int,IN `_afectacion_compra_id` int,IN `_precio_venta` decimal(11,2),IN `_precio_compra` decimal(11,2),IN `_codigo_interno` varchar(255),IN `_codigo_sunat` varchar(8),IN `_has_igv` char(1), IN _fecha_proceso datetime, IN _fecha_transaccion datetime)
BEGIN
	#Routine body goes here...

INSERT INTO `producto` (
  `descripcion` ,
  `unidad_medida_id`,
  `moneda_id`,
  `afectacion_venta_id` ,
  `afectacion_compra_id`,
  `precio_venta` ,
  `precio_compra`,
  `codigo_interno`,
  `codigo_sunat` ,
  `has_igv` ,
  `fecha_proceso`,
  `fecha_transaccion`,
  `marca_baja`

)
 VALUES (

_descripcion,
 _unidad_medida_id,
 _moneda_id,
 _afectacion_venta_id ,
 _afectacion_compra_id,
 _precio_venta,
 _precio_compra,
_codigo_interno,
_codigo_sunat ,
_has_igv ,
 _fecha_proceso,
 _fecha_transaccion,
	0

);



END;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for
-- `pa_set_estado_comprobante`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  29-01-2021
-- Sistema: Facturador ElectrÃ³nico
-- Modulo: General
-- DescripciÃ³n: cambia estado de comprobante
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_set_estado_comprobante`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_set_estado_comprobante`(IN `_id` int, IN `_id_estado` char(2))
BEGIN
	#Routine body goes here...
update comprobante
set id_estado=_id_estado
where id= _id;
END;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for
-- `pa_buscar_codigo_sunat_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  05-02-2021
-- Sistema: Facturador ElectrÃ³nico
-- Modulo: General
-- DescripciÃ³n: busca los codigo producto sunat  
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_buscar_codigo_sunat_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_codigo_sunat_producto`(IN flag int,IN _cod_buscar int, IN _cod_clase int, IN _descripcion varchar(255))
BEGIN
	#Routine body goes here...
	IF flag = 1 then
	    SELECT id, descripcion FROM codsntsegmento WHERE marca_baja=0;
      END IF;
	
	IF flag = 2 then
	    SELECT id, descripcion FROM codsntfamilia WHERE id_segmento=_cod_buscar and marca_baja=0;
      END IF;

	IF flag = 3 then
	    SELECT id, descripcion FROM codsntclase WHERE id_familia=_cod_buscar and marca_baja=0;
      END IF;

	IF flag = 4 then
	    SELECT id, descripcion FROM codsntproducto WHERE id_clase=_cod_buscar and marca_baja=0;
      END IF;

	IF flag = 5 then
	
	    SELECT p.id, p.descripcion FROM codsntproducto  p
		WHERE  if(_cod_clase=0, p.id_clase  like "%%", p.id_clase =_cod_clase )
		and 	p.descripcion like  CONCAT('%', _descripcion  , '%')
		  and p.marca_baja=0;
      END IF;
END;;
DELIMITER ;



-- ----------------------------
--  Procedure definition for `pa_search_soap`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  20-08-2021
-- Sistema: Facturador ElectrÃ³nico
-- Modulo: General
-- DescripciÃ³n: busqueda de usuario y password soap
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_search_soap`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_soap`( )
BEGIN
		select usuario_soap, clave_soap from empresa where id=1;
		   			

 END

;;
DELIMITER ;

