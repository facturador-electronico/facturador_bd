-- ----------------------------
--  Procedure definition for `pa_get_hash`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: devuelve campo hash de comprobante por id
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_get_hash`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_get_hash`(IN `_id` int)
BEGIN
	#Routine body goes here...
select hash from comprobante where id =_id;
END
;;
DELIMITER ;
