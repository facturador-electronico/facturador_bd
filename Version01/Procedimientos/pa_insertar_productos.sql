
-- ----------------------------
--  Procedure definition for
-- `pa_insertar_productos`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: inserta datos en la tabla productos
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_insertar_productos`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insertar_productos`(IN `_descripcion` varchar(255),IN `_unidad_medida_id` varchar(20),IN `_moneda_id` varchar(20),IN `_afectacion_venta_id` int,IN `_afectacion_compra_id` int,IN `_precio_venta` decimal(11,2),IN `_precio_compra` decimal(11,2),IN `_codigo_interno` varchar(255),IN `_codigo_sunat` varchar(8),IN `_has_igv` char(1), IN _fecha_proceso datetime, IN _fecha_transaccion datetime)
BEGIN
	#Routine body goes here...

INSERT INTO `producto` (
  `descripcion` ,
  `unidad_medida_id`,
  `moneda_id`,
  `afectacion_venta_id` ,
  `afectacion_compra_id`,
  `precio_venta` ,
  `precio_compra`,
  `codigo_interno`,
  `codigo_sunat` ,
  `has_igv` ,
  `fecha_proceso`,
  `fecha_transaccion`,
  `marca_baja`

)
 VALUES (

_descripcion,
 _unidad_medida_id,
 _moneda_id,
 _afectacion_venta_id ,
 _afectacion_compra_id,
 _precio_venta,
 _precio_compra,
_codigo_interno,
_codigo_sunat ,
_has_igv ,
 _fecha_proceso,
 _fecha_transaccion,
	0

);



END;;
DELIMITER ;
