
-- ----------------------------
--  Procedure definition for
-- `pa_eliminar_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: da de baja un producto
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_eliminar_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_eliminar_producto`( IN _id int, IN _fecha_transaccion datetime)
BEGIN
	update `producto` set marca_baja= 9, fecha_transaccion= _fecha_transaccion where id=_id;
END;;
DELIMITER ;
