----------------------------------------------------------------------------------
-- Author: jeffrey garcia - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: Listar conceptos generales
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `pa_listar_conceptos`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_conceptos`(IN `flag` INT)
begin
	
	IF flag = 1 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0 and correlativo<07 order by correlativo ASC;

      END IF;
	

	  IF flag = 2 then

		    SELECT concat(ruc,"|",ruta_dowland,"|",razon_social,"|",telefono,"|",celular,"|",direccion) as descripcion FROM empresa limit 1;
   
	   END IF;

	    IF flag = 3 then

		    SELECT parametro as descripcion FROM parametro where concepto_id = 1 and marca_baja = 0;
  
	    END IF;
  
	    IF flag = 4 then

		    SELECT parametro as descripcion FROM parametro where concepto_id = 2 and marca_baja = 0;
  
	    END IF;


	IF flag = 5 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0 and correlativo >06  and correlativo<09 order by correlativo ASC;

      END IF;

	IF flag = 6 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =20  and parametro2 ="01"and marca_baja = 0;

      END IF;

	IF flag = 7 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =20  and parametro2 ="03"and marca_baja = 0;

      END IF;

	IF flag = 8 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =21  and parametro2 ="01"and marca_baja = 0;

      END IF;

	IF flag = 9 then

	     SELECT parametro as descripcion FROM parametro where concepto_id =21  and parametro2 ="03"and marca_baja = 0;

      END IF;
	IF flag = 10 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 2   AND marcaBaja = 0;

      END IF;
	IF flag = 11 then

	    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 3   AND marcaBaja = 0 ;

      END IF;

	IF flag = 12 then

		    SELECT correlativo, descripcion FROM tblConcepto WHERE codConcepto = 1   AND marcaBaja = 0  order by correlativo ASC;

      END IF;

	IF flag = 13 then
		   SELECT  parametro2 as codigo, parametro as descripcion  FROM `parametro`where concepto_id=28 and marca_baja=0;
      END IF;

	IF flag = 14 then
		   SELECT parametro2 as codigo, parametro as descripcion  FROM `parametro`where concepto_id=29 and marca_baja=0;
      END IF;
	IF flag = 15 then
		   SELECT parametro2 as codigo, parametro as descripcion, par_tipo as free  FROM `parametro`where concepto_id=30 and marca_baja=0;
      END IF;
	IF flag = 16 then
		   SELECT   par_tipo as free  FROM `parametro`where concepto_id=32 and marca_baja=0;
      END IF;
    IF flag = 17 then
		SELECT GROUP_CONCAT(par_int1) FROM `parametro` where concepto_id=33 and marca_baja=0;
	END IF;
	IF flag = 18 then
		    SELECT  par_tipo as activoCodigoSunat  FROM `parametro`where concepto_id=34 and marca_baja=0;
    END IF;
	IF flag = 19 then
	    SELECT  par_tipo as activoPrdCatVenta  FROM `parametro`where concepto_id=35 and marca_baja=0;
	 END IF;
end
;;
DELIMITER ;
