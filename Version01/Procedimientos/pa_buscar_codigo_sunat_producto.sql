-- ----------------------------
--  Procedure definition for
-- `pa_buscar_codigo_sunat_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  05-02-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: busca los codigo producto sunat  
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_buscar_codigo_sunat_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_codigo_sunat_producto`(IN flag int,IN _cod_buscar int, IN _cod_clase int, IN _descripcion varchar(255))
BEGIN
	#Routine body goes here...
	IF flag = 1 then
	    SELECT id, descripcion FROM codsntsegmento WHERE marca_baja=0;
      END IF;
	
	IF flag = 2 then
	    SELECT id, descripcion FROM codsntfamilia WHERE id_segmento=_cod_buscar and marca_baja=0;
      END IF;

	IF flag = 3 then
	    SELECT id, descripcion FROM codsntclase WHERE id_familia=_cod_buscar and marca_baja=0;
      END IF;

	IF flag = 4 then
	    SELECT id, descripcion FROM codsntproducto WHERE id_clase=_cod_buscar and marca_baja=0;
      END IF;

	IF flag = 5 then
	
	    SELECT p.id, p.descripcion FROM codsntproducto  p
		WHERE  if(_cod_clase=0, p.id_clase  like "%%", p.id_clase =_cod_clase )
		and 	p.descripcion like  CONCAT('%', _descripcion  , '%')
		  and p.marca_baja=0;
      END IF;
END;;
DELIMITER ;
