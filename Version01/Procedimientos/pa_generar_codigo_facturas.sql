----------------------------------------------------------------------------------
-- Author: jeffrey garcia - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: Generar código de comprobante
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `generar_codigo_facturas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `generar_codigo_facturas`(`_tipo_comprobante` VARCHAR(5))
BEGIN
	
    DECLARE cant int; 
    
    select count(tipo_comprobante) + 1  INTO cant from comprobante 
    where tipo_comprobante = _tipo_comprobante;
    
    
	SELECT lpad(cant,8,0);

END
;;
DELIMITER ;
