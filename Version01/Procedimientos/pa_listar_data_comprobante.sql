-- --------------------------------------------------------------------------------
-- Author: jeffrey garcia - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripci�n: Listar datos de comprobante
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `pa_listar_data_comprobante`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_data_comprobante`(`_id` INT)
BEGIN
SELECT
 '0101' as TipoOperacion,									-- (01 : Tipo de operación) Catálogo N° 51 , 0101 - venta interna
       cast( cab.fecha_emision as char(10)) as FechaEmision,	-- (02 : Fecha de emisión YYYY-MM-DD
	   '05:02:12' AS HoraEmision,								-- (03 : Hora de Emisión ) HH:MM:SS
	   case when cab.fecha_vencimiento is null then '-' 
	   else cast( cab.fecha_vencimiento as char(10)) End
	   as FechaVencimiento,										-- (04 :Fecha de vencimiento ) YYYY-MM-DD
	   '0000' CodDomicilioFiscal,								-- (05 : Código del domicilio fiscal o de local anexo del emisor )
	   cab.tipo_documento as TipoDocCliente,				    -- (06 : Tipo de documento de identidad del adquirente o usuario) Catálogo N° 6
	    cab.nom_cliente as nomCustomer,	
		cab.num_documento as docCustomer,						-- (08 : Apellidos y nombres, denominación o razón social del adquirente o usuario   )
	   p.parametro2 as tipo_moneda,									-- (09 : Tipo de moneda en la cual se emite la factura electrónica) Catálogo N° 2
	   cab.igv + cab.isc as SumTotalTributos,			        -- (10 : Sumatoria Tributos)
	   cab.subtotal as TotalValorVenta,						    -- (11 : Total valor de venta )
	   cab.importe as TotalPrecioVenta,					        -- (12 : Total Precio de Venta)
	   (cab.otros_descuentos + cab.descuento_global) as TotalDescuentos,	-- (13 : Total descuentos (no afectan la base imponible del IGV/IVAP))
	  '0.00' as otrosCargos,
      '0.00' as TotalAnticipos,				                        -- (15 : Total Anticipos)
	   cab.importe as ImporteTotalVenta,				        -- (16 : Importe total de la venta, cesión en uso o del servicio prestado, suma de 12-13+14-15)
	   '2.1' as VersionUBL,										-- (17 : Versión UBL)
	   '2.0' as Customizacion,	                                -- (18 : Customization Documento) 

		cab.codigo as seriesAfectado,
		cab.dir_cliente as direccionCliente,
		cab.tipo_comprobante as tipoComprobante

       FROM comprobante as cab, empresa as emp, parametro as p
where emp.id = cab.empresa_id and cab.id = _id  and p.concepto_id = 28
and p.parametro2 = cab.tipo_moneda;


Select 
	'NIU' as UnidadMedida,	               
       	dt.cantidad as cantidad,						     
	IF(dt.codigo_producto="","-",dt.codigo_producto) as codigoProducto,					
	 IF(dt.codigo_sunat IS NULL,"-",dt.codigo_sunat) as CodigoProdSunat,					
	 dt.producto as Producto,		            
	 dt.valor_item as ValorUnitario,	
			      
	ROUND( (dt.precio_unitario * dt.cantidad * 0.18/1.18),2) as SumTribxItem,	/*cambios mal calculado*/
	  
    
	   '1000' as CodTribIGV,		            
	   ROUND(dt.valor_item * 0.18,2) as MontoIGVItem,	
	   dt.valor_item as baseImponible,						
	   'IGV' as nombreTributo,	

		'VAT' as codAfecIGV,			      

	   '10' as tributoAfectacion,				                
	   '18.00' as tributoPorcentaje ,								

	  
	    '-' as codigTributoISC,					
	    0 as mtoISCxitem,		-- (16 :
		0 as BaseImpISC,									
		'' as NomTribxItemISC,					
		'' as CodTiposTributoISC,					
		-- CodTipoTributoISC,Tributo ISC: Porcentaje de ISC

		'' as TipoSistemaISC,								
		'' as PorcImoISC,									
		-- Tributo Otro 9999
		'-' as CodTipoTribOtros,							
		0 as MtoTribOTrosxItem,							
		0 as BaseImpOtroxItem,							
		'' as NomTribOtroxItem,							
		'' as CodTipTribOtroxItem,						
		0 as PorTribOtroXItem,							
		-- Tributo ICBPER 7152
		'-' as codTriIcbper,							
		'' as mtoTriIcbperItem,						
		'' as ctdBolsasTriIcbperItem,				
		'' as nomTributoIcbperItem,					
		'' as codTipTributoIcbperItem,				
		'' as mtoTriIcbperUnidad ,					
		--

		(dt.precio_unitario ) as PrecioVtaUnitario,		
		
		dt.valor_item as valor_item	,						
		0 as ValorRefUnitario_Gratuito,

		 ROUND(dt.precio_unitario/1.18,2 ) as valorBaseItem	,
		dt.unidad_medida as unidadMedidaNota,
		dt.codigo_producto as codigoProductoNota				
FROM detalle_comprobante as dt,comprobante as cmp
where dt.comprobante_id = cmp.id and dt.comprobante_id = _id;
end
;;
DELIMITER ;
