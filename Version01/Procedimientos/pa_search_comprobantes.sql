
-- ----------------------------
--  Procedure definition for `pa_search_comprobantes`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: busqueda de comprobantes para aplicar nota
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_search_comprobantes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_comprobantes`(
 IN  codigo_venta  int,
 IN series_number varchar(50),

 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 )
BEGIN
		if series_number is NULL OR series_number='' then
			SELECT 
			c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
			 FROM `comprobante` as c
			where c.tipo_comprobante in ('01','03') and 

			 c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )
			and
			( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			order by c.id desc;
 		else 
			SELECT 
			c.id as VENTA,  c.codigo as DOCUMENTO ,c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
			 FROM `comprobante`c
			where 
			c.tipo_comprobante in ('01','03') and		
			 c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )		

			and	c.codigo  = series_number 	
			and   if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   ) order by c.id desc;

		end if ;
						   			

 END

;;
DELIMITER ;
