
-- ----------------------------
--  Procedure definition for `pa_search_comprobantes_fechas`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: busqueda por fechas de comprobantes para aplicar 
-- nota
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
DROP PROCEDURE IF EXISTS `pa_search_comprobantes_fechas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_comprobantes_fechas`(
 IN fecha_inicio   varchar(50),   
IN fecha_fin  varchar(50),
 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 , IN codigo_venta int,  IN series_number varchar(50))
BEGIN
		SELECT 
			c.id as VENTA, c.codigo as DOCUMENTO,c.fecha_emision as FECHA, c.nom_cliente as CLIENTE
		 FROM `comprobante` AS c
		where 
		c.tipo_comprobante in ('01','03') 
		and   c.id not in ( (select affected_document_id from notes where affected_document_id =c.id) )
		and	( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
		and  c.fecha_emision  between fecha_inicio and fecha_fin 
		and	if( (series_number is null || series_number="") , c.codigo like"%%" ,c.codigo  = series_number ) 
		and   if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   ) 
		order by c.id desc;
	
END
;;
DELIMITER ;
