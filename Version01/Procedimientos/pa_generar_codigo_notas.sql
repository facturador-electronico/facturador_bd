-- ----------------------------
--  Procedure definition for `pa_generar_codigo_notas`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: Generar código para notas
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_generar_codigo_notas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_generar_codigo_notas`(IN `_tipo_comprobante` varchar(5),IN `_inicial` char(1))
BEGIN
	
    DECLARE cant int; 
    
    select count(tipo_comprobante) + 1  INTO cant from comprobante 
    where tipo_comprobante = _tipo_comprobante and codigo  like CONCAT('%', _inicial , '%');
    
    
	SELECT lpad(cant,8,0);

END
;;
DELIMITER ;