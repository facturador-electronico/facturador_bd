
-- ----------------------------
--  Procedure definition for
-- `pa_buscar_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: busqueda de producto para listado y edicion
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_buscar_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_producto`(IN `_descripcion` varchar(255),IN `_codigo_interno` varchar(255), IN `_id` int)
BEGIN
	#Routine body goes here...
	SELECT 

p.codigo_interno,
p.descripcion,
(SELECT  parametro   FROM  parametro where concepto_id=29  and parametro2=p.unidad_medida_id)  as unidadMedida,
(SELECT  parametro   FROM  parametro where concepto_id=28  and parametro2=p.moneda_id)  as moneda,
(SELECT  parametro   FROM  parametro where concepto_id=30  and parametro2=p.afectacion_venta_id) as moneda,
p.precio_venta,
if(p.has_igv="1","SI","NO") as conIGV,
p.id, 
p.codigo_sunat, 
p.unidad_medida_id, 
p.moneda_id,
p.afectacion_venta_id
 FROM `producto` p
where 	
 p.descripcion REGEXP   _descripcion 
and p.codigo_interno like  CONCAT('%', _codigo_interno , '%')
and 
if(_id=0,p.id like "%%", p.id=_id)
and p.marca_baja=0
order by p.id desc;
 	
END;;
DELIMITER ;

