
-- ----------------------------
--  Procedure definition for `pa_search_soap`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  20-08-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: busqueda de usuario y password soap
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_search_soap`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_soap`( )
BEGIN
		select usuario_soap, clave_soap from empresa where id=1;
		   			

 END

;;
DELIMITER ;