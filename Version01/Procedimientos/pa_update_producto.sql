
-- ----------------------------
--  Procedure definition for
-- `pa_update_producto`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  22-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: actualiza datos de tabla producto
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_update_producto`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_update_producto`(IN `_descripcion` varchar(255),IN `_unidad_medida_id` varchar(20),IN `_moneda_id` varchar(20),IN `_afectacion_venta_id` int,IN `_afectacion_compra_id` int,IN `_precio_venta` decimal(11,2),IN `_precio_compra` decimal(11,2),IN `_codigo_interno` varchar(255),IN `_codigo_sunat` varchar(8),IN `_has_igv` char(1), IN _id int, IN _fecha_transaccion datetime)
BEGIN
	#Routine body goes here...


update `producto`
 set
descripcion = _descripcion,
unidad_medida_id = _unidad_medida_id,
moneda_id =_moneda_id,
afectacion_venta_id= _afectacion_venta_id ,
afectacion_compra_id =_afectacion_compra_id,
precio_venta = _precio_venta,
precio_compra=_precio_compra,
codigo_interno=_codigo_interno,
codigo_sunat=_codigo_sunat ,
has_igv= _has_igv ,
fecha_transaccion= _fecha_transaccion


where id=_id;

END;;
DELIMITER ;
