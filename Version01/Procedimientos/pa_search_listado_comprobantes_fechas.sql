
-- ----------------------------
--  Procedure definition for
-- `pa_search_listado_comprobantes_fechas`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  19-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: busqueda entre fechas de comprobantes para recrear PDF
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_search_listado_comprobantes_fechas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_listado_comprobantes_fechas`(
 IN fecha_inicio   varchar(50),   
IN fecha_fin  varchar(50),
 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 , IN _tipo_comprobante char(2),IN series_number varchar(50), IN codigo_venta int, IN _id_estado char(2))
BEGIN

		SELECT 
				c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE,
  				CASE c.id_estado
          			 	WHEN '01'       THEN 'REGISTRADO'
          				 WHEN '02'       THEN 'XML GENERADO'
          				 WHEN '03'       THEN 'ENVIADO'
          				 WHEN '05'       THEN 'ACEPTADO'
					 WHEN '07'       THEN 'OBSERVADO'
					 WHEN '09'       THEN 'RECHAZADO'
		       END AS ESTADO, c.tipo_moneda as MONEDA,
			if( c.subtotal <0  , (c.subtotal*-1)  , c.subtotal ) as GRAVADO, 
			if( c.igv <0  , (c.igv*-1)  , c.igv ) as IGV,
			if( c.importe <0  , (c.importe*-1)  , c.importe ) as TOTAL,
			(select descripcion from tblconcepto where codConcepto=1 and correlativo=c.tipo_comprobante ) as TIPO,
			c.tipo_comprobante as TIPO_COMPROBANTE
		 FROM `comprobante` AS c
		where 
		( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and  if(series_number ="",c.codigo like "%%",c.codigo  = series_number )
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			and  c.fecha_emision  between fecha_inicio and fecha_fin
			and  if(_tipo_comprobante="0", c.tipo_comprobante  like"%%", c.tipo_comprobante =_tipo_comprobante)

			and  if(_id_estado ="",c.id_estado like "%%",c.id_estado  = _id_estado  )
			  order by  c.id  desc;

END
;;
DELIMITER ;

