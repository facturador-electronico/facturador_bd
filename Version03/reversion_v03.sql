DELETE FROM `tblconcepto` WHERE (`codConcepto`='1005');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1006');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1007');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1008');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1009');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1010');

DELETE FROM `tblconcepto` WHERE (`codConcepto`='1011');
DELETE FROM `tblconcepto` WHERE (`codConcepto`='1000');

DELETE FROM `parametro` WHERE (`concepto_id`='1005');
DELETE FROM `parametro` WHERE (`concepto_id`='1006');
DELETE FROM `parametro` WHERE (`concepto_id`='1007');
DELETE FROM `parametro` WHERE (`concepto_id`='1008');
DELETE FROM `parametro` WHERE (`concepto_id`='1009');
DELETE FROM `parametro` WHERE (`concepto_id`='1010');
DELETE FROM `parametro` WHERE (`concepto_id`='1011');
DELETE FROM `parametro` WHERE (`concepto_id`='1000');

DROP TABLE IF EXISTS resumen;
DROP TABLE IF EXISTS resumen_documento;




DROP PROCEDURE IF EXISTS `pa_search_listado_comprobantes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_listado_comprobantes`(
 IN  codigo_venta  int,
 IN series_number varchar(50),

 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
, IN _tipo_comprobante char(2), IN _id_estado char(2))
BEGIN

			SELECT 
			c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE, 
			CASE c.id_estado
          				 WHEN '01'       THEN 'REGISTRADO'
          				 WHEN '02'       THEN 'XML GENERADO'
          				 WHEN '03'       THEN 'ENVIADO'
          				 WHEN '05'       THEN 'ACEPTADO'
					 WHEN '07'       THEN 'OBSERVADO'
					 WHEN '09'       THEN 'RECHAZADO'
		       END AS ESTADO,
			 c.tipo_moneda as MONEDA,
			if( c.subtotal <0  , (c.subtotal*-1)  , c.subtotal ) as GRAVADO, 
			if( c.igv <0  , (c.igv*-1)  , c.igv ) as IGV,
			if( c.importe <0  , (c.importe*-1)  , c.importe ) as TOTAL,
			(select descripcion from tblconcepto where codConcepto=1 and correlativo=c.tipo_comprobante ) as TIPO,
			c.tipo_comprobante as TIPO_COMPROBANTE

			 FROM `comprobante` as c
			where (c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and  if(series_number ="",c.codigo like "%%",c.codigo  = series_number )
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			and  if(_tipo_comprobante="0", c.tipo_comprobante  like"%%", c.tipo_comprobante =_tipo_comprobante)

			and  if(_id_estado ="",c.id_estado like "%%",c.id_estado  = _id_estado  )

			order by c.id desc;
 	

						   			

 END
;;
DELIMITER ;


DROP PROCEDURE IF EXISTS `pa_search_listado_comprobantes_fechas`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_search_listado_comprobantes_fechas`(
 IN fecha_inicio   varchar(50),   
IN fecha_fin  varchar(50),
 IN  dni_ruc varchar(50),
IN name_cliente varchar(150)
 , IN _tipo_comprobante char(2),IN series_number varchar(50), IN codigo_venta int, IN _id_estado char(2))
BEGIN

		SELECT 
				c.id as VENTA,c.codigo as DOCUMENTO, c.fecha_emision as FECHA, c.nom_cliente as CLIENTE,
  				CASE c.id_estado
          			 	WHEN '01'       THEN 'REGISTRADO'
          				 WHEN '02'       THEN 'XML GENERADO'
          				 WHEN '03'       THEN 'ENVIADO'
          				 WHEN '05'       THEN 'ACEPTADO'
					 WHEN '07'       THEN 'OBSERVADO'
					 WHEN '09'       THEN 'RECHAZADO'
		       END AS ESTADO, c.tipo_moneda as MONEDA,
			if( c.subtotal <0  , (c.subtotal*-1)  , c.subtotal ) as GRAVADO, 
			if( c.igv <0  , (c.igv*-1)  , c.igv ) as IGV,
			if( c.importe <0  , (c.importe*-1)  , c.importe ) as TOTAL,
			(select descripcion from tblconcepto where codConcepto=1 and correlativo=c.tipo_comprobante ) as TIPO,
			c.tipo_comprobante as TIPO_COMPROBANTE
		 FROM `comprobante` AS c
		where 
		( c.num_documento like  CONCAT('%', dni_ruc , '%')   and   c.nom_cliente like CONCAT('%', name_cliente , '%'))
			and  if(series_number ="",c.codigo like "%%",c.codigo  = series_number )
			and if(codigo_venta=0 , c.id like"%%" ,c.id=codigo_venta   )
			and  c.fecha_emision  between fecha_inicio and fecha_fin
			and  if(_tipo_comprobante="0", c.tipo_comprobante  like"%%", c.tipo_comprobante =_tipo_comprobante)

			and  if(_id_estado ="",c.id_estado like "%%",c.id_estado  = _id_estado  )
			  order by  c.id  desc;




		
	
END
;;


alter table detalle_comprobante modify precio_unitario decimal(11,2);
alter table detalle_comprobante modify descuento_unitario decimal(11,2);
alter table detalle_comprobante modify valor_item decimal(11,2);




DROP PROCEDURE IF EXISTS `pa_listar_data_comprobante`;

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_listar_data_comprobante`(`_id` INT)
BEGIN
SELECT
 '0101' as TipoOperacion,									-- (01 : Tipo de operación) Catálogo N° 51 , 0101 - venta interna
       cast( cab.fecha_emision as char(10)) as FechaEmision,	-- (02 : Fecha de emisión YYYY-MM-DD
	   '05:02:12' AS HoraEmision,								-- (03 : Hora de Emisión ) HH:MM:SS
	   case when cab.fecha_vencimiento is null then '-' 
	   else cast( cab.fecha_vencimiento as char(10)) End
	   as FechaVencimiento,										-- (04 :Fecha de vencimiento ) YYYY-MM-DD
	   '0000' CodDomicilioFiscal,								-- (05 : Código del domicilio fiscal o de local anexo del emisor )
	   cab.tipo_documento as TipoDocCliente,				    -- (06 : Tipo de documento de identidad del adquirente o usuario) Catálogo N° 6
	    cab.nom_cliente as nomCustomer,	
		cab.num_documento as docCustomer,						-- (08 : Apellidos y nombres, denominación o razón social del adquirente o usuario   )
	   p.parametro2 as tipo_moneda,									-- (09 : Tipo de moneda en la cual se emite la factura electrónica) Catálogo N° 2
	   cab.igv + cab.isc as SumTotalTributos,			        -- (10 : Sumatoria Tributos)
	   cab.subtotal as TotalValorVenta,						    -- (11 : Total valor de venta )
	   cab.importe as TotalPrecioVenta,					        -- (12 : Total Precio de Venta)
	   (cab.otros_descuentos + cab.descuento_global) as TotalDescuentos,	-- (13 : Total descuentos (no afectan la base imponible del IGV/IVAP))
	  '0.00' as otrosCargos,
      '0.00' as TotalAnticipos,				                        -- (15 : Total Anticipos)
	   cab.importe as ImporteTotalVenta,				        -- (16 : Importe total de la venta, cesión en uso o del servicio prestado, suma de 12-13+14-15)
	   '2.1' as VersionUBL,										-- (17 : Versión UBL)
	   '2.0' as Customizacion,	                                -- (18 : Customization Documento) 

		cab.codigo as seriesAfectado,
		cab.dir_cliente as direccionCliente,
		cab.tipo_comprobante as tipoComprobante

       FROM comprobante as cab, empresa as emp, parametro as p
where emp.id = cab.empresa_id and cab.id = _id  and p.concepto_id = 28
and p.parametro2 = cab.tipo_moneda;


Select 
	 (SELECT parametro2 FROM `parametro`where concepto_id=29 and marca_baja=0 and parametro= dt.unidad_medida limit 1 ) as UnidadMedida,	                
       	dt.cantidad as cantidad,						     
	IF(dt.codigo_producto="","-",dt.codigo_producto) as codigoProducto,					
	 IF(dt.codigo_sunat IS NULL,"-",dt.codigo_sunat) as CodigoProdSunat,					
	 dt.producto as Producto,		            
	 dt.valor_item as ValorUnitario,	
			      
	ROUND( (dt.precio_unitario * dt.cantidad * 0.18/1.18),2) as SumTribxItem,	/*cambios mal calculado*/
	  
    
	   '1000' as CodTribIGV,		            
	   ROUND(dt.valor_item * 0.18,2) as MontoIGVItem,	
	   dt.valor_item as baseImponible,						
	   'IGV' as nombreTributo,	

		'VAT' as codAfecIGV,			      

	   '10' as tributoAfectacion,				                
	   '18.00' as tributoPorcentaje ,								

	  
	    '-' as codigTributoISC,					
	    0 as mtoISCxitem,		-- (16 :
		0 as BaseImpISC,									
		'' as NomTribxItemISC,					
		'' as CodTiposTributoISC,					
		-- CodTipoTributoISC,Tributo ISC: Porcentaje de ISC

		'' as TipoSistemaISC,								
		'' as PorcImoISC,									
		-- Tributo Otro 9999
		'-' as CodTipoTribOtros,							
		0 as MtoTribOTrosxItem,							
		0 as BaseImpOtroxItem,							
		'' as NomTribOtroxItem,							
		'' as CodTipTribOtroxItem,						
		0 as PorTribOtroXItem,							
		-- Tributo ICBPER 7152
		'-' as codTriIcbper,							
		'' as mtoTriIcbperItem,						
		'' as ctdBolsasTriIcbperItem,				
		'' as nomTributoIcbperItem,					
		'' as codTipTributoIcbperItem,				
		'' as mtoTriIcbperUnidad ,					
		--

		(dt.precio_unitario ) as PrecioVtaUnitario,		
		
		dt.valor_item as valor_item	,						
		0 as ValorRefUnitario_Gratuito,

		 ROUND(dt.precio_unitario/1.18,2 ) as valorBaseItem	,
		dt.unidad_medida as unidadMedidaNota,
		dt.codigo_producto as codigoProductoNota				
FROM detalle_comprobante as dt,comprobante as cmp
where dt.comprobante_id = cmp.id and dt.comprobante_id = _id;
end
;;


