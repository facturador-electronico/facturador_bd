-- ----------------------------
--  Procedure definition for `pa_update_hash`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: actualiza campo hash en comprobante
-- nota
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_update_hash`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_update_hash`(IN `_id` int,IN `_hash` varchar(255))
BEGIN
	#Routine body goes here...
update comprobante set hash=_hash where id=_id;
END
;;
DELIMITER ;