-- ----------------------------
--  Procedure definition for `pa_insert_notes`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  15-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: inserta en tabla notes 
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_insert_notes`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_insert_notes`(
IN document_id INT,
IN note_type varchar(50),
IN note_credit_type_id varchar(50),
IN note_debit_type_id varchar(50),
IN note_description varchar(255),
IN affected_document_id INT
 , IN marca_baja char(1), IN fecha_transaccion date, IN fecha_proceso date, IN hora_proceso time, IN usuario varchar(255), IN importe decimal(12,2))
BEGIN

INSERT INTO `notes` (
`document_id`, 
`importe`,
`note_type`,
`note_credit_type_id`,
`note_debit_type_id`, 
`note_description`, 
`affected_document_id`, 
`marca_baja`,
`fecha_transaccion`,
`fecha_proceso`,
`hora_proceso`,
`usuario`

)
 VALUES (

document_id,
importe,
note_type,
note_credit_type_id,
note_debit_type_id,
note_description,
affected_document_id,
marca_baja,
fecha_transaccion,
fecha_proceso,
hora_proceso,
usuario

);

END
;;
DELIMITER ;