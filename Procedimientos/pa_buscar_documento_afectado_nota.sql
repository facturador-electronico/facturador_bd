-- ----------------------------
--  Procedure definition for `pa_buscar_documento_afectado_nota`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  19-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: buscad el documento afectado por nota, y descripcion de anulacion
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_buscar_documento_afectado_nota`;
DELIMITER ;;

CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_buscar_documento_afectado_nota`(IN `_id` int)
BEGIN


select 
c.codigo as codigo_afectado, 
if(n.note_credit_type_id is null, "", 
(SELECT  descripcion   FROM `tblconcepto` where codConcepto=3  and correlativo=n.note_credit_type_id) ) as reason_credit,
if(n.note_debit_type_id is null, "", 
(SELECT  descripcion   FROM `tblconcepto` where codConcepto=2  and correlativo=n.note_debit_type_id)) as reason_debit
from 
notes n 
left join comprobante c on  n.affected_document_id=c.id
where n.document_id=_id;

END;;
DELIMITER ;
