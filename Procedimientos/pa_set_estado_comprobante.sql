-- ----------------------------
--  Procedure definition for `pa_insert_notes`
-- ----------------------------
-- --------------------------------------------------------------------------------
-- Author:  juan alarcon - IDE-SOLUTION
-- Created:  29-01-2021
-- Sistema: Facturador Electrónico
-- Modulo: General
-- Descripción: cambia estado de comprobante
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO          NOMBRE                    FECHA       MOTIVO  
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------

DROP PROCEDURE IF EXISTS `pa_set_estado_comprobante`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `pa_set_estado_comprobante`(IN `_id` int, IN `_id_estado` char(2))
BEGIN
	#Routine body goes here...
update comprobante
set id_estado=_id_estado
where id= _id;
END
;;
DELIMITER ;