INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2000' , N'0' , N'' , N'PSE -  USUARIO  DE INICIO DE SESSION JWT',  N'0');
INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2001' , N'0' , N'' , N'PSE -  PASSWORD  DE INICIO DE SESSION JWT',  N'0');
INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2002' , N'0' , N'' , N'PSE -  TOKEN GENERADO',  N'0');

INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2003' , N'0' , N'' , N'PSE -  URL SERVICIO GENERARCOMPROBANTE-WEBAPI',  N'0');
INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2004' , N'0' , N'' , N'PSE -  API SERVICIO GENERAR TOKEN - WEB API',  N'0');
INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2005' , N'0' , N'' , N'PSE -  API SERVICIO GENERAR COMPROBANTE - WEB API',  N'0');
INSERT INTO `tblconcepto` (`ntraConcepto`, `codConcepto`, `correlativo`, `decripcion2`, `descripcion`, `marcaBaja`) VALUES (NULL, N'2006' , N'0' , N'' , N'PSE -  API SERVICIO CONSULTAR ESTADOS CPE - WEB API',  N'0');

INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2000', N'idesolution$pse' , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');
INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2001', N'Marzo@2022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');
INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2002', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');


INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2003', N'http://ec2-3-209-229-40.compute-1.amazonaws.com:8000' , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');
INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2004', N'/api/IniciarSession', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');
INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2005', N'/api/GenerarComprobanteDoc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');
INSERT INTO `parametro` (`id`, `concepto_id`, `parametro`, `parametro2`, `par_int1`, `par_int2`, `par_float1`, `par_float2`, `par_date1`, `par_date2`, `par_tipo`, `marca_baja`) VALUES (NULL, N'2006', N'/api/ConsultarSituacionCpe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0');



ALTER TABLE `parametro` MODIFY parametro  VARCHAR(65535) DEFAULT NULL;



DROP TABLE IF EXISTS response_comprobante;
CREATE TABLE response_comprobante (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comprobante_id` int(11) NOT NULL,
  `response` varchar(65535) NOT NULL,
  `url_pdf` varchar(65535) NULL,
  `url_xml` varchar(65535) NULL,
  `url_cdr` varchar(65535) NULL,
  `marca_baja` smallint NOT NULL DEFAULT 0,
  `fecha_proceso` date DEFAULT NOW(),
  `hora_proceso` time DEFAULT DATE_FORMAT(NOW(), "%H:%i:%S"),
  `fecha_creacion_url` datetime,
  `fecha_vencimiento_url` datetime,
  `usuario` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4;


UPDATE `empresa` SET `ruta_dowland`='C:\\PSEv1\\' WHERE (`id`='1');

